//
//  ViewController.h
//  CallPhoneExample
//
//  Created by Apple on 3/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *tfUserID;
@property (weak, nonatomic) IBOutlet UISwitch *switchVideoCall;
@property (weak, nonatomic) IBOutlet UIButton *buttonCall;

- (IBAction)callTapped:(UIButton *)sender;

@end

