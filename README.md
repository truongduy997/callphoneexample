# CallPhoneExample

- Documents with Stringee Call API using iOS SDK

    - link: https://developer.stringee.com/docs/call-api-getting-started-ios

* Create new project with Objective-C
* Install Stringee with CocoaPods
 
        pod 'Stringee'

* Then from the command line run:

        pod install --repo-update

* In the "Build Settings" tab -> "Other linker flags" add "$(inherited)" flag
* The "Build Settings" tab -> "Enable bitcode" select "NO"

* Edit Info.plist, right-click the information property list file (Info.plist) and select Open As -> Source Code

        <key>NSCameraUsageDescription</key>
        <string>$(PRODUCT_NAME) uses Camera</string>
        <key>NSMicrophoneUsageDescription</key>
        <string>$(PRODUCT_NAME) uses Microphone</string>
        
* Create file StringeeImplement, add a line to import the Stringee library:

        #import <Stringee/Stringee.h>

* Implements the StringeeConnectionDelegate protocol in .h file
    

        @interface ViewController : UIViewController <StringeeConnectionDelegate>

        @property(strong, nonatomic) StringeeClient * stringeeClient;

        @end
        
* In .m file 

        - (void)requestAccessToken:(StringeeClient *)stringeeClient {
            NSLog(@"requestAccessToken");
        }

        - (void)didConnect:(StringeeClient *)stringeeClient isReconnecting:(BOOL)isReconnecting {
            NSLog(@"Successfully connected to Stringee Server, user ID: %@", stringeeClient.userId);
        }

        - (void)didDisConnect:(StringeeClient *)stringeeClient isReconnecting:(BOOL)isReconnecting {
            NSLog(@"didDisConnect");
        }

        - (void)didFailWithError:(StringeeClient *)stringeeClient code:(int)code message:(NSString *)message {
            NSLog(@"Failed connection to Stringee Server with error: %@", message);
        }
        
* Start connecting Stringee sever with accessToken

        NSString* accessToken = @"...";

        self.stringeeClient = [[StringeeClient alloc] initWithConnectionDelegate:self];
        [self.stringeeClient connectWithAccessToken:accessToken];
        
* Make a call

        - (void)didConnect:(StringeeClient *)stringeeClient isReconnecting:(BOOL)isReconnecting {
            NSLog(@"Successfully connected to Stringee Server, user ID: %@", stringeeClient.userId);

            StringeeCall * stringeeCall = [[StringeeCall alloc] initWithStringeeClient:self.stringeeClient isIncomingCall:NO from:@"84273065979" to:@"84909982888"];
            // completion handle call
            [stringeeCall makeCallWithCompletionHandler:^(BOOL status, int code, NSString *message, NSString *data) {
                NSLog(@"Make call result: code=%d, message: %@, data: %@" ,code, message, data);
                if (!status) {
                  // Nếu make call không thành công thì kết thúc cuộc gọi
                  [self endCallAndDismissWithTitle:@"Cuộc gọi không thành công"];
                }
            }];
        }

* Implements the StringeeCallDelegate protocol

        #import <Stringee/Stringee.h>
    
        @interface CallingViewController : UIViewController<StringeeCallDelegate>
        
        @end

* Receiving call events

        //----------- StringeeCallDelegate -->

        - (void)didChangeSignalingState:(StringeeCall *)stringeeCall signalingState:(SignalingState)signalingState reason:(NSString *)reason sipCode:(int)sipCode sipReason:(NSString *)sipReason {
            NSLog(@"*********Callstate: %ld", (long)signalingState);
    
            dispatch_async(dispatch_get_main_queue(), ^{
                switch (signalingState) {
                
                    case SignalingStateCalling:
                        self.labelConnecting.hidden = NO;
                        self.labelConnecting.text = @"Đang gọi...";
                        break;
                
                    case SignalingStateRinging:
                        self.labelConnecting.text = @"Đang đổ chuông...";
                        break;
                
                    case SignalingStateAnswered: {
                        self->hasAnsweredCall = YES;
                        if (self->hasConnectedMedia) {
                            [self startTimer];
                        } else {
                            self.labelConnecting.text = @"Đang kết nối...";
                        }
                    } break;
                
                    case SignalingStateBusy: {
                        [self stopTimer];
                        [self endCallAndDismissWithTitle:@"Số máy bận"];
                    } break;
                
                    case SignalingStateEnded: {
                        [self stopTimer];
                        [self endCallAndDismissWithTitle:@"Kết thúc cuộc gọi"];
                    } break;
                
                }
            });
        }
        
*  Hanging up a call

         [stringeeCall hangupWithCompletionHandler:^(BOOL status, int code, NSString *message) {
            NSLog(@"%@", message);
        }];
        
*  Reject a call

        [stringeeCall rejectWithCompletionHandler:^(BOOL status, int code, NSString *message) {
            NSLog(@"%@", message);
        }];









